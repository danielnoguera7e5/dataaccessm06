package cat.itb.danielnoguera7e5.dam.m06.uf1.repas

import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.coroutines.DEBUG_PROPERTY_NAME
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import java.util.Scanner


@Serializable
data class personalInfo (
//    @SerialName("name") val name: String,
    @SerialName("age") val age: String,
//    @SerialName("country_id") val country_id: String
        )

 suspend fun apiAge(name: String){
    val client = HttpClient(CIO){
        install(ContentNegotiation) {
            json(Json {
                ignoreUnknownKeys = true
            })
        }
    }
    val person: personalInfo = client.get("https://api.agify.io/?name=${name}&country_id=ES").body()
}


//fun main() {
//    val scanner = Scanner(System.`in`)
//    println("Com et dius?")
//    val name = scanner.nextLine()
//    val age = apiAge(name)
//}











