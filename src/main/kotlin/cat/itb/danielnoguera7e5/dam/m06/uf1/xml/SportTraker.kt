package cat.itb.danielnoguera7e5.dam.m06.uf1.xml

import kotlinx.serialization.SerialName
import nl.adaptivity.xmlutil.serialization.XML
import nl.adaptivity.xmlutil.serialization.XmlElement
import java.nio.file.Path
import java.util.*
import kotlin.io.path.Path
import kotlin.io.path.exists
import kotlin.io.path.readText
import kotlin.io.path.writeText

@kotlinx.serialization.Serializable
@SerialName("esports")
data class Esports(val esport: MutableList<Esport>)

@kotlinx.serialization.Serializable
@SerialName("esport")
data class Esport(val duracio: String,
                  @XmlElement(true) val nom: String)

fun pathSportXml(): Path {
    return Path("/home/sjo/Escriptori/DADES/sportTraker.xml")
}

fun readUserSport(nom: String, duracio: String): Esports {
    return Esports(mutableListOf(Esport(duracio, nom)))
}

fun writeSportXml(nom: String, duracio: String) {
    var esports = readUserSport(nom, duracio)
    if (pathSportXml().exists()) {
        esports = readSportXml()
        esports.esport.add(readUserSport(nom, duracio).esport[0])
    }
    val xmlEsports = XML.encodeToString(esports)
    pathSportXml().writeText(xmlEsports)
}

fun readSportXml(): Esports {
    val xml = pathSportXml().readText()
    return XML.decodeFromString<Esports>(xml)
}

fun duracioTotal() {
    val duracio = readSportXml().esport.sumOf { it.duracio.toInt() }
    println("Duració total: $duracio")
}

fun duracioTotalPerEsport() {
    val mapEsports = readSportXml().esport.groupBy { it.nom }
    for ((key, value) in mapEsports) {
        println("${key}: ${value.sumOf { it.duracio.toInt() }}")
    }
}

fun main() {
    val scanner = Scanner(System.`in`)
    writeSportXml(scanner.nextLine(), scanner.nextLine())
    duracioTotal()
    duracioTotalPerEsport()
}