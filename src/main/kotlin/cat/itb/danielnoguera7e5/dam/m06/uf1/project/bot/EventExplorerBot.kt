package cat.itb.danielnoguera7e5.dam.m06.uf1.project.bot

import cat.itb.danielnoguera7e5.dam.m06.uf1.project.data.repository.EventRepository
import com.github.kotlintelegrambot.Bot
import com.github.kotlintelegrambot.bot
import com.github.kotlintelegrambot.dispatch
import com.github.kotlintelegrambot.dispatcher.command
import com.github.kotlintelegrambot.entities.ChatId

class EventExplorerBot(val eventRepository: EventRepository) {
    companion object {
        const val telegramKey = "5601661485:AAFVyGV-J3SSPFgEYqZcYao65jzrW99geHw"
    }

    fun setupBot(): Bot {
        return bot {
            token = telegramKey
            dispatch {
                setupCommands()
            }
        }
    }

    private fun com.github.kotlintelegrambot.dispatcher.Dispatcher.setupCommands() {
        command("add") {
            val joinedArgs = args.joinToString(separator = " ")
//            val response = eventUseCases.add(joinedArgs)
//            bot.sendMessage(chatId = ChatId.fromId(message.chat.id), text = response)
        }
        command("info") {

        }
    }
}
