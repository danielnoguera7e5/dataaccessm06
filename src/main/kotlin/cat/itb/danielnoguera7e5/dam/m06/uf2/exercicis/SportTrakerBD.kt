package cat.itb.danielnoguera7e5.dam.m06.uf2.exercicis

import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.util.*

object SportsTable: IntIdTable() {
    val name = varchar("name", 50)
    val duration = integer("duration")
}

fun main() {
    val scanner = Scanner(System.`in`)
    Database.connect("jdbc:h2:./sportTracker", "org.h2.Driver")
    transaction {
        addLogger(StdOutSqlLogger)
        SchemaUtils.create (SportsTable)
        SportsTable.insert {
            it[name] = scanner.nextLine()
            it[duration] = scanner.next().toInt()
        }
        val duracio = SportsTable.selectAll().sumOf { it[SportsTable.duration] }
        println(duracio)

        val mapEsports = SportsTable.selectAll().groupBy { it[SportsTable.name] }
        for ((key, value) in mapEsports) {
            println("${key}: ${value.sumOf { it[SportsTable.duration] }}")
        }
    }
}
