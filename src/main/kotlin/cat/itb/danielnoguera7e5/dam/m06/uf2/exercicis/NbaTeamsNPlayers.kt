package cat.itb.danielnoguera7e5.dam.m06.uf2.exercicis

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.isNotNull
import org.jetbrains.exposed.sql.transactions.transaction
import java.util.*


object teams: Table() {
    val name = varchar("name", 20)
    val city = varchar("city", 20)
    val conference = varchar("conference", 4)
    val division = varchar("division", 9)
    override val primaryKey by lazy { super.primaryKey ?: PrimaryKey(name) }
}

object players: Table() {
    val id = integer("id")
    val name = varchar("name", 30)
    val origin = varchar("origin", 20)
    val height = varchar("height", 4)
    val weight = integer("weight")
    val position = varchar("position", 12)
    val nameiteam = varchar("nameiteam", 20)
        .references(teams.name)
}

fun main() {
    val scanner = Scanner(System.`in`)
    Database.connect("jdbc:postgresql://localhost:5432/nba_teams_n_players",
        driver = "org.postgresql.Driver", user = "sjo", password = "sjo")
    transaction {
        addLogger(StdOutSqlLogger)

//        var allTeamsName = "[ "
//        teams.slice(teams.name).selectAll().map { allTeamsName += ("${it[teams.name]} ") }
//        allTeamsName += "]"
//        println(allTeamsName)
//
//        val namePlayer = scanner.nextLine()
//        players.selectAll().filter {it[players.name] == namePlayer}.forEach { println(it[players.origin]) }
//
//        (players innerJoin teams)
//            .slice(players.height.max(), teams.division)
//            .select { players.nameiteam eq teams.name }
//            .groupBy(teams.division)
//            .forEach { println("${it[players.height.max()]} - ${it[teams.division]}") }
//     IMC = (PES / ALTURA * ALTURA) * 703

//        fun imc(weight: Int, height: Double): Int {return weight / ()}
//        players
//            .selectAll()


//        teams.slice(players.height.max(), teams.name, teams.division).selectAll().groupBy(players.nameiteam).forEach { println(it[players.height]) }
    }
}