package cat.itb.danielnoguera7e5.dam.m06.uf1.project

import cat.itb.danielnoguera7e5.dam.m06.uf1.project.bot.EventExplorerBot
import cat.itb.danielnoguera7e5.dam.m06.uf1.project.data.repository.EventRepository


fun main() {
    val repository = EventRepository()
    val bot = EventExplorerBot(repository).setupBot()
    bot.startPolling()
}
