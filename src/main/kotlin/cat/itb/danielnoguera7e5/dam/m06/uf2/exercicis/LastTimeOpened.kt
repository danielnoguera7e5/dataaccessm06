package cat.itb.danielnoguera7e5.dam.m06.uf2.exercicis

import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.LocalDateTime


object DatesOpenedTable: IntIdTable() {
    val date = varchar("date", 50)
}

fun main() {
    val db = Database.connect("jdbc:h2:./myh2file", "org.h2.Driver")
    transaction {
        addLogger(StdOutSqlLogger)
        SchemaUtils.create (DatesOpenedTable)

        val newDate = DatesOpenedTable.insert {
            it[date] = LocalDateTime.now().toString()
        } get DatesOpenedTable.id

        DatesOpenedTable.selectAll().forEach {
            println(it[DatesOpenedTable.date])
        }
    }
}