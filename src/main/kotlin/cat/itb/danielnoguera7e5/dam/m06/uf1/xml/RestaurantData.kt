package cat.itb.danielnoguera7e5.dam.m06.uf1.xml

import kotlinx.serialization.SerialName
import nl.adaptivity.xmlutil.serialization.XML
import nl.adaptivity.xmlutil.serialization.XmlElement
import kotlin.io.path.Path
import kotlin.io.path.readText


@kotlinx.serialization.Serializable
@SerialName("restaurant")
data class Restaurant(val type: String, @XmlElement(true) val name: String,
                      @XmlElement(true) val address: String, @XmlElement(true) val owner: String)

fun main() {
    val xml = Path("/home/sjo/Escriptori/DADES/restaurant.xml").readText()
    val xmlRestaurant: Restaurant = XML.decodeFromString(xml)
    println(xmlRestaurant)
}