package cat.itb.danielnoguera7e5.dam.m06.uf1.project.data.repository

import cat.itb.danielnoguera7e5.dam.m06.uf1.project.data.model.Embedded
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.json.Json


// API READER

class EventRepository() {
    companion object {
        const val KEY = "7elxdku9GGG5k8j0Xm8KWdANDgecHMV0"
    }
    private val client = HttpClient(CIO) {
        install(ContentNegotiation) {
            json(Json {
                ignoreUnknownKeys = true
            })
        }
    }
    suspend fun listEvents(): Embedded {
        return client.get("https://app.ticketmaster.com/discovery/v2/events?") {
            parameter("apikey", KEY)
            parameter("locale", "*")
            parameter("size", "10")
        }.body()
    }
}