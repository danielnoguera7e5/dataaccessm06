package cat.itb.danielnoguera7e5.dam.m06.uf1.repas

import java.nio.file.Path
import kotlin.io.path.createFile
import kotlin.io.path.listDirectoryEntries
import kotlin.io.path.writeText


fun homePath(): Path {
    val homePath = System.getProperty("user.home")
    return Path.of(homePath)
}

fun filePath(): Path {
    return homePath().resolve("desktopContents.txt")
}

fun findNumberOfFiles(): Int {
    val desktopPath = Path.of("/home/sjo/Escriptori")
    return desktopPath.listDirectoryEntries().size
}

fun writeNumberOfFiles(numberOfFiles: Int, filePath: Path) {
    filePath.writeText("A /Escriptori hi ha un total de $numberOfFiles fitxers")
}

fun main() {
    filePath().createFile()
    writeNumberOfFiles(findNumberOfFiles(), filePath())
}