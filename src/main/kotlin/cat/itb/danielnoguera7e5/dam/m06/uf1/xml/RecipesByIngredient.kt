package cat.itb.danielnoguera7e5.dam.m06.uf1.xml

import kotlinx.serialization.SerialName
import nl.adaptivity.xmlutil.serialization.XML
import nl.adaptivity.xmlutil.serialization.XmlElement
import java.util.*
import kotlin.io.path.Path
import kotlin.io.path.readText

@kotlinx.serialization.Serializable
@SerialName("recipes")
data class Recipes(val recipe: List<Recipe>)

@kotlinx.serialization.Serializable
@SerialName("recipe")
data class Recipe(val dificulty: String,
                  @XmlElement(true) val name: String,
                  @XmlElement(true) val ingredients: Ingredients)

@kotlinx.serialization.Serializable
@SerialName("ingredients")
data class Ingredients(val ingredient: List<Ingredient>)

@kotlinx.serialization.Serializable
@SerialName("ingredient")
data class Ingredient(val ammount: String,
                      val unit: String,
                      val name: String)

fun readRecipes(): Recipes {
    val xml = Path("/home/sjo/Escriptori/DADES/receptes.xml").readText()
    return XML.decodeFromString(xml)
}

fun filterByIngredientName(name: String): List<Recipe> {
    val filteredList = readRecipes().recipe.filter { it -> it.ingredients.ingredient.any { it.name == name } }
    return filteredList.sortedByDescending { it.dificulty }
}

fun main() {
    val scanner = Scanner(System.`in`)
    println(filterByIngredientName(scanner.nextLine()))
}