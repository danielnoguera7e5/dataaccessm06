package cat.itb.danielnoguera7e5.dam.m06.uf2.exercicis.nba

import cat.itb.danielnoguera7e5.dam.m06.uf2.exercicis.nba.player.Companion.referrersOn
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SqlExpressionBuilder.isNotNull
import org.jetbrains.exposed.sql.StdOutSqlLogger
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.addLogger
import org.jetbrains.exposed.sql.transactions.transaction


object Teams: Table() {
    val name = varchar("name", 20)
    val city = varchar("city", 20)
    val conference = varchar("conference", 4)
    val division = varchar("division", 9)
    override val primaryKey by lazy { super.primaryKey ?: PrimaryKey(name) }
}

object Players: IntIdTable() {
    val name = varchar("name", 30)
    val origin = varchar("origin", 20)
    val height = varchar("height", 4)
    val weight = integer("weight")
    val position = varchar("position", 12)
    val nameiteam = varchar("nameiteam", 20)
}

class player(id: EntityID<Int>): IntEntity(id) {
    companion object : IntEntityClass<player>(Players)
    var name by Players.name
//    var nameiteam by Teams referencedOn Players.nameiteam
}

fun main() {
    Database.connect("jdbc:postgresql://localhost:5432/nba_teams_n_players",
        driver = "org.postgresql.Driver", user = "sjo", password = "sjo")
    transaction {
        addLogger(StdOutSqlLogger)
    }
}