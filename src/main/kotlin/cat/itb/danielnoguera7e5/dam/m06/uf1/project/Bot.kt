package cat.itb.danielnoguera7e5.dam.m06.uf1.project

import cat.itb.danielnoguera7e5.dam.m06.uf1.project.data.repository.EventRepository
import com.github.kotlintelegrambot.bot
import com.github.kotlintelegrambot.dispatch
import com.github.kotlintelegrambot.dispatcher.command
import com.github.kotlintelegrambot.entities.ChatId

fun main() {
    val eventRepository = EventRepository()
    val ticketMasterRepository = TicketMasterRepository()
    val eventUseCases = EventUseCases(eventRepository, ticketMasterRepository)
    val bot = bot {
        token = "5601661485:AAFVyGV-J3SSPFgEYqZcYao65jzrW99geHw"
        dispatch {
            command("add") {
                val joinedArgs = args.joinToString(separator = " ")
                val response = eventUseCases.add(joinedArgs)
                bot.sendMessage(chatId = ChatId.fromId(message.chat.id), text = response)
            }
            command("info") {

            }
        }
    }
    bot.startPolling()
}