package cat.itb.danielnoguera7e5.dam.m06.uf1.project

import cat.itb.danielnoguera7e5.dam.m06.uf1.project.data.model.Event
import cat.itb.danielnoguera7e5.dam.m06.uf1.project.data.repository.EventRepository

// BUSINESS LOGIC (GENERAL)

class EventUseCases(
    val eventRepository: EventRepository,
    val ticketMasterRepository: TicketMasterRepository) {

    fun add(args: String): String {
        val argsList = args.split(";").map { it.trim() }
        ticketMasterRepository.addEvent(Event(argsList[1], argsList[2], argsList[3]))
        return if (args.isNotBlank()) "Event ${argsList[1]} with id ${argsList[2]} added."
        else "There is no text apart from command!"
    }
    suspend fun info(): String {
        var response = ""
        eventRepository.listEvents().events.forEach {
            response += "Id:${it.id} Name:${it.name} Type:${it.type}\n"
        }
        return response
    }

}
