package cat.itb.danielnoguera7e5.dam.m06.uf1.project.data.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


@Serializable
@SerialName("_embedded")
data class Embedded(@SerialName("events") val events: List<Event>)

@Serializable
data class Event(@SerialName("name") val name: String,
                 @SerialName("type") val type: String,
                 @SerialName("id") val id: String
//                 @SerialName("startDateTime") val startDateTime: String,
//                 @SerialName("endDateTime") val endDateTime: String,
//                 @SerialName("onsaleStartDateTime") val onsaleStartDateTime: String,
//                 @SerialName("onsaleEndDateTime") val onsaleEndDateTime: String,
//                 @SerialName("city") val city: String,
//                 @SerialName("countryCode") val countryCode: String)
){

//    fun getCountryName(): String {
//        var countryName = ""
//        countryName = when (countryCode) {
//            "US" -> "United States Of America"
//            "AD" -> "Andorra"
//            "AI" -> "Anguilla"
//            "AR" -> "Argentina"
//            "AU" -> "Australia"
//            "AT" -> "Austria"
//            "AZ" -> "Azerbaijan"
//            "BS" -> "Bahamas"
//            "BH" -> "Bahrain"
//            "BB" -> "Barbados"
//            "BE" -> "Belgium"
//            "BM" -> "Bermuda"
//            "BR" -> "Brazil"
//            "BG" -> "Bulgaria"
//            "CA" -> "Canada"
//            "CL" -> "Chile"
//            "CN" -> "China"
//            "CO" -> "Colombia"
//            "CR" -> "Costa Rica"
//            "HR" -> "Croatia"
//            "CY" -> "Cyprus"
//            "CZ" -> "Czech Republic"
//            "DK" -> "Denmark"
//            "DO" -> "Dominican Republic"
//            "EC" -> "Ecuador"
//            "EE" -> "Estonia"
//            "FO" -> "Faroe Islands"
//            "FI" -> "Finland"
//            "FR" -> "France"
//            "GE" -> "Georgia"
//            "DE" -> "Germany"
//            "GH" -> "Ghana"
//            "GI" -> "Gibraltar"
//            "GB" -> "Great Britain"
//            "GR" -> "Greece"
//            "HK" -> "Hong Kong"
//            "HU" -> "Hungary"
//            "IS" -> "Iceland"
//            "IN" -> "India"
//            "IE" -> "Ireland"
//            "IL" -> "Israel"
//            "IT" -> "Italy"
//            "JM" -> "Jamaica"
//            "JP" -> "Japan"
//            "KR" -> "Korea"
//            "LV" -> "Latvia"
//            "LB" -> "Lebanon"
//            "LT" -> "Lithuania"
//            "LU" -> "Luxembourg"
//            "MY" -> "Malaysia"
//            "MT" -> "Malta"
//            "MX" -> "Mexico"
//            "MC" -> "Monaco"
//            "ME" -> "Montenegro"
//            "MA" -> "Morocco"
//            "NL" -> "Netherlands"
//            "AN" -> "Netherlands Antilles"
//            "NZ" -> "New Zealand"
//            "ND" -> "Northern Ireland"
//            "NO" -> "Norway"
//            "PE" -> "Peru"
//            "PL" -> "Poland"
//            "PT" -> "Portugal"
//            "RO" -> "Romania"
//            "RU" -> "Russian Federation"
//            "LC" -> "Saint Lucia"
//            "SA" -> "Saudi Arabia"
//            "RS" -> "Serbia"
//            "SG" -> "Singapore"
//            "SK" -> "Slovakia"
//            "SI" -> "Slovenia"
//            "ZA" -> "South Africa"
//            "ES" -> "Spain"
//            "SE" -> "Sweden"
//            "CH" -> "Switzerland"
//            "TW" -> "Taiwan"
//            "TH" -> "Thailand"
//            "TT" -> "Trinidad and Tobago"
//            "TR" -> "Turkey"
//            "UA" -> "Ukraine"
//            "AE" -> "United Arab Emirates"
//            "UY" -> "Uruguay"
//            "VE" -> "Venezuela"
//            else -> countryCode
//        }
//        return countryName
//    }

}