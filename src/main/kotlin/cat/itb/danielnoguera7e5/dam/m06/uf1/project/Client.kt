package cat.itb.danielnoguera7e5.dam.m06.uf1.project


import cat.itb.danielnoguera7e5.dam.m06.uf1.project.data.model.Embedded
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.json.Json


suspend fun client(): Embedded {
    val client = HttpClient(CIO) {
        install(ContentNegotiation) {
            json(Json {
                ignoreUnknownKeys = true
                prettyPrint = true
            })
        }
    }
    val apiKey = "7elxdku9GGG5k8j0Xm8KWdANDgecHMV0"
    return client.get("https://app.ticketmaster.com/discovery/v2/events?apikey=${apiKey}&locale=*").body()
}


