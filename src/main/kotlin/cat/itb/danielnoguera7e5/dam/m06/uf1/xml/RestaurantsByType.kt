package cat.itb.danielnoguera7e5.dam.m06.uf1.xml

import kotlinx.serialization.SerialName
import nl.adaptivity.xmlutil.serialization.XML
import nl.adaptivity.xmlutil.serialization.XmlElement
import java.util.*
import kotlin.io.path.Path
import kotlin.io.path.readText

@kotlinx.serialization.Serializable
@SerialName("restaurants")
data class Restaurants( @XmlElement(true) val restaurant: List<Restaurant>)

fun readXml(): Restaurants {
    val xml = Path("/home/sjo/Escriptori/DADES/restaurants.xml").readText()
    return XML.decodeFromString(xml)
}

fun printSameType(type: String) {
    for (restaurant in readXml().restaurant) {
        if (restaurant.type == type) {
            println("${restaurant.name} - ${restaurant.address} - ${restaurant.owner}")
        }
    }
}

fun main() {
    val scanner = Scanner(System.`in`)

    println("Quin tipus de restaurant vols?")
    printSameType(scanner.nextLine())
}